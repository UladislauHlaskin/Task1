﻿using System;
using AirCompany.Airlines;
using AirCompany.Planes;
using AirCompany.Employees;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Airline epamAirlines = GetAirline();
            string menuSelection = "";
            do
            {
                Console.Clear();
                Console.WriteLine("1 - вывести самолеты авиакомпании.");
                Console.WriteLine("2 - вывести общую вместимость пассажиров всех самолетов.");
                Console.WriteLine("3 - вывести общую грузовую вместимость самолетов.");
                Console.WriteLine("4 - вывести отсортированные самолеты по дальности полета.");
                Console.WriteLine("5 - найти самолет по диапозону потребления горючего.");
                Console.WriteLine("6 - выход.");
                Console.Write("Введите пункт меню: ");
                menuSelection = Console.ReadLine();
                switch(menuSelection)
                {
                    case "1":
                        Console.Write(epamAirlines);
                        Console.ReadKey();
                        break;
                    case "2":
                        Console.WriteLine($"Общая вместимость пассажиров всех самолетов - {epamAirlines.TotalPassengerCapacity}");
                        Console.ReadKey();
                        break;
                    case "3":
                        Console.WriteLine($"Общая грузовая вместимость всех самолетов - {epamAirlines.TotalCargoCapacity}кг");
                        Console.ReadKey();
                        break;
                    case "4":
                        Console.Write(epamAirlines.GetSortedPlanes());
                        Console.ReadKey();
                        break;
                    case "5":
                        Console.Write("Введите минимальный расход топлива: ");
                        double fuelConsumptionMin = !double.TryParse(Console.ReadLine(), out fuelConsumptionMin) ? 0 : fuelConsumptionMin;
                        Console.Write("Введите максимальный расход топлива: ");
                        double fuelConsumptionMax = !double.TryParse(Console.ReadLine(), out fuelConsumptionMax) ? 1000 : fuelConsumptionMax;
                        var plane = epamAirlines.GetPlaneByFuelConsumption(fuelConsumptionMin, fuelConsumptionMax);
                        if (plane != null)
                        {
                            Console.WriteLine(plane);
                        }
                        else
                        {
                            Console.WriteLine("Самолет с заданными параметрами не найден.");
                        }
                        Console.ReadKey();
                        break;
                    case "6":
                        return;
                    default:
                        Console.WriteLine("Выберите корректный пункт меню.");
                        Console.ReadKey();
                        break;
                }    
            }
            while (true);
        }

        private static Airline GetAirline()
        {
            Airline airline = new Airline("Epam airlines");
            PlaneModel boeing747 = new PlaneModel("Boeing", "747", SkillLevel.Advanced);
            PlaneModel airbusA380 = new PlaneModel("Airbus", "A380", SkillLevel.Advanced);
            PlaneModel an225 = new PlaneModel("Ан", "225", SkillLevel.Expert);
            PlaneModel cessna400 = new PlaneModel("Cessna", "400", SkillLevel.Beginner);
            PlaneModel cessnaT37 = new PlaneModel("Cessna", "T-37", SkillLevel.Trainee);
            airline.Planes.Add(new PassengerPlane("123-f214-a415sa", boeing747));
            airline.Planes.Add(new PassengerPlane("444-f214-a415sa", boeing747, 532, 4000, 150, 1000)); // старый побитый жизнью самолет
            airline.Planes.Add(new PassengerPlane("444-h31a-t31", airbusA380, 300, 4500, 200, 2000));
            airline.Planes.Add(new CargoPlane("t21-192q-111", an225));
            airline.Planes.Add(new LightPlane("213-142-521", cessna400));
            airline.Planes.Add(new TrainingPlane("t31-as3-1wa", cessnaT37));
            return airline;
        }
    }
}
