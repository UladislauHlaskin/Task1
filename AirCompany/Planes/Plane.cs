﻿using AirCompany.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 * Реализовать консольное приложение, удовлетворяющее следующим требованиям:

1.  Выполнить анализ и декомпозицию предметной области.  30 %
2.	Использовать возможности ООП: наследование, полиморфизм, инкапсуляция.Отразить декомпозицию в структуре классов.  30 %
3.	Каждый класс должен иметь исчерпывающее смысл название и информативный состав.  15 %
4.	При кодировании следует придерживаться соглашения об оформлении кода code convention. (TO DO, FAQ) 15 %
5.  Файлы проекта должны быть разделены по папкам согласно доменной модели. 5 %
6.	Работа с консолью или консольное меню должно быть минимальным. 5 %

* Авиакомпания. 
* Определить иерархию самолетов. 
* Создать авиакомпанию. Посчитать общую вместимость и грузоподъемность. 
* Провести сортировку самолетов компании по дальности полета. 
* Найти самолет в компании, соответствующий заданному диапазону параметров потребления горючего.
*/
namespace AirCompany.Planes
{
    public abstract class Plane
    {
        public string UniqueNumber { get; }
        public PlaneModel PlaneModel { get; }
        public double CargoCapacity { get; set; }
        public int PassengerCapacity { get; set; }
        public double FuelConsumption { get; set; }
        public double TankCapacity { get; set; }
        public PlaneType PlaneType { get; protected set; }
        public double MaxFlyDistance => Math.Round(TankCapacity / FuelConsumption) * 1000;
        protected abstract int _maxPassengerCapacity { get; }
        protected abstract double _maxCargoCapacity { get; }
        protected abstract double _defaultFuelConsumption { get; }
        protected abstract double _defaultTankCapacity { get; }
        public Plane()
        { }
        public Plane(string uniqueNumber, PlaneModel planeModel)
        {
            UniqueNumber = uniqueNumber;
            PlaneModel = planeModel;
        }

        //public void Fly(Pilot pilot, double distance)
        //{
        //    int pilotSkillLevel = (int)pilot.SkillLevel;
        //    int skillLevelRequired = (int)PlaneModel.SkillLevelRequired;
        //    if (skillLevelRequired > pilotSkillLevel)
        //    {
        //        throw new ArgumentException("The pilot isn't skilled enough.");
        //    }    
        //}
    }
}
