﻿using System;
using System.Globalization;

namespace AirCompany.Planes
{
    public class TrainingPlane : Plane
    {
        protected override int _maxPassengerCapacity => 0;
        protected override double _maxCargoCapacity => 30;
        protected override double _defaultFuelConsumption => 30;
        protected override double _defaultTankCapacity => 240;
        public TrainingPlane(string uniqueNumber, PlaneModel planeModel) : base(uniqueNumber, planeModel)
        {
            PlaneType = PlaneType.Training;
            PassengerCapacity = _maxPassengerCapacity;
            CargoCapacity = _maxCargoCapacity;
            FuelConsumption = _defaultFuelConsumption;
            TankCapacity = _defaultTankCapacity;
        }
        public TrainingPlane
            (string uniqueNumber, PlaneModel planeModel, double fuelConsumption, double tankCapacity, double cargoCapacity)
            : this(uniqueNumber, planeModel)
        {
            if (cargoCapacity > _maxCargoCapacity)
            {
                throw new ArgumentException($"Training plane cannot have {cargoCapacity}kg cargo capacity, max value is {_maxCargoCapacity}kg.");
            }
            FuelConsumption = fuelConsumption;
            TankCapacity = tankCapacity;
            CargoCapacity = cargoCapacity;
        }
        public override string ToString()
        {
            return 
                $"Training plane - {PlaneModel}, max fly distance = {MaxFlyDistance}, fuel consumption = {FuelConsumption.ToString(CultureInfo.InvariantCulture)}";
        }
    }
}
