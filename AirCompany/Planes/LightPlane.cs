﻿using System;
using System.Globalization;

namespace AirCompany.Planes
{
    public class LightPlane : Plane
    {
        protected override int _maxPassengerCapacity => 5;
        protected override double _maxCargoCapacity => _maxPassengerCapacity * 300;
        protected override double _defaultFuelConsumption => 60;
        protected override double _defaultTankCapacity => 500;
        public LightPlane(string uniqueNumber, PlaneModel planeModel) : base(uniqueNumber, planeModel)
        {
            PlaneType = PlaneType.Light;
            PassengerCapacity = _maxPassengerCapacity;
            CargoCapacity = _maxCargoCapacity;
            FuelConsumption = _defaultFuelConsumption;
            TankCapacity = _defaultTankCapacity;
        }
        public LightPlane
            (string uniqueNumber, PlaneModel planeModel, double fuelConsumption, double tankCapacity, int passengerCapacity, double cargoCapacity)
            : this(uniqueNumber, planeModel)
        {
            if (passengerCapacity > _maxPassengerCapacity)
            {
                throw new ArgumentException($"Light plane cannot have {passengerCapacity} passenger capacity, max value is {_maxPassengerCapacity}kg.");
            }
            if (cargoCapacity > _maxCargoCapacity)
            {
                throw new ArgumentException($"Light plane cannot have {cargoCapacity}kg cargo capacity, max value is {_maxCargoCapacity}kg.");
            }
            PassengerCapacity = passengerCapacity;
            FuelConsumption = fuelConsumption;
            TankCapacity = tankCapacity;
            CargoCapacity = cargoCapacity;
        }
        public override string ToString()
        {
            return
                $"Light plane - {PlaneModel}, max fly distance = {MaxFlyDistance}, fuel consumption = {FuelConsumption.ToString(CultureInfo.InvariantCulture)}";
        }
    }
}
