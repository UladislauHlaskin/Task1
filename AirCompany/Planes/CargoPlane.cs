﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirCompany.Planes
{
    public class CargoPlane : Plane
    {
        protected override int _maxPassengerCapacity => 5;
        protected override double _maxCargoCapacity => 10000;
        protected override double _defaultFuelConsumption => 1000;
        protected override double _defaultTankCapacity => 5000;
        public CargoPlane(string uniqueNumber, PlaneModel planeModel) : base(uniqueNumber, planeModel)
        {
            PlaneType = PlaneType.Cargo;
            PassengerCapacity = _maxPassengerCapacity;
            CargoCapacity = _maxCargoCapacity;
            FuelConsumption = _defaultFuelConsumption;
            TankCapacity = _defaultTankCapacity;
        }
        public CargoPlane
            (string uniqueNumber, PlaneModel planeModel, double fuelConsumption, double tankCapacity, int passengerCapacity, double cargoCapacity)
            : this(uniqueNumber, planeModel)
        {
            if (passengerCapacity > _maxPassengerCapacity)
            {
                throw new ArgumentException($"Cargo plane cannot have {passengerCapacity} passenger capacity, max value is {_maxPassengerCapacity}kg.");
            }
            if (cargoCapacity > _maxCargoCapacity)
            {
                throw new ArgumentException($"Cargo plane cannot have {cargoCapacity}kg cargo capacity, max value is {_maxCargoCapacity}kg.");
            }
            PassengerCapacity = passengerCapacity;
            FuelConsumption = fuelConsumption;
            TankCapacity = tankCapacity;
            CargoCapacity = cargoCapacity;
        }
        public override string ToString()
        {
            return
                $"Cargo plane - {PlaneModel}, max fly distance = {MaxFlyDistance}, fuel consumption = {FuelConsumption.ToString(CultureInfo.InvariantCulture)}";
        }
    }
}
