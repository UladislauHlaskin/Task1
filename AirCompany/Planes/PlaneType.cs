﻿namespace AirCompany.Planes
{
    public enum PlaneType
    {
        Training,
        Cargo,
        Light,
        Passenger
    }
}
