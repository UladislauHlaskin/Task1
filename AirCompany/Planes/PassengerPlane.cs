﻿using System;
using System.Globalization;

namespace AirCompany.Planes
{
    public class PassengerPlane : Plane
    {
        protected override int _maxPassengerCapacity => 300;
        protected override double _maxCargoCapacity => _maxPassengerCapacity * _maxCargoPerPassenger;
        private double _maxCargoPerPassenger => 30;
        protected override double _defaultFuelConsumption => 500;
        protected override double _defaultTankCapacity => 5000;
        public PassengerPlane(string uniqueNumber, PlaneModel planeModel) : base(uniqueNumber, planeModel)
        {
            PlaneType = PlaneType.Passenger;
            PassengerCapacity = _maxPassengerCapacity;
            CargoCapacity = _maxCargoCapacity;
            FuelConsumption = _defaultFuelConsumption;
            TankCapacity = _defaultTankCapacity;
        }
        public PassengerPlane
            (string uniqueNumber, PlaneModel planeModel, double fuelConsumption, double tankCapacity, int passengerCapacity, double cargoCapacity)
            : this(uniqueNumber, planeModel)
        {
            if (passengerCapacity > _maxPassengerCapacity)
            {
                throw new ArgumentException($"Passenger plane cannot have {passengerCapacity} passenger capacity, max value is {_maxPassengerCapacity}kg.");
            }

            double maxCargo = _maxCargoPerPassenger * _maxPassengerCapacity;
            if (cargoCapacity > maxCargo)
            {
                throw new ArgumentException($"Passenger plane cannot have {cargoCapacity}kg cargo capacity, max value is {maxCargo}kg.");
            }
            PassengerCapacity = passengerCapacity;
            FuelConsumption = fuelConsumption;
            TankCapacity = tankCapacity;
            CargoCapacity = cargoCapacity;
        }
        public override string ToString()
        {
            return
                $"Passenger plane - {PlaneModel}, max fly distance = {MaxFlyDistance}, fuel consumption = {FuelConsumption.ToString(CultureInfo.InvariantCulture)}";
        }
    }
}
