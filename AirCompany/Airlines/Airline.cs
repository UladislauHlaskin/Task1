﻿using System.Collections.Generic;
using System.Linq;
using AirCompany.Planes;
using AirCompany.Employees;
using System.Text;

namespace AirCompany.Airlines
{
    public class Airline
    {
        public string Name { get; set; }
        public List<Plane> Planes { get; set; }
        public List<Employee> Employees { get; set; }
        public Airline()
        {
        }
        public Airline(string name)
        {
            Name = name;
            Planes = new List<Plane>();
            Employees = new List<Employee>();
        }
        public Airline(string name, List<Plane> planes, List<Employee> employees)
        {
            Name = name;
            Planes = planes;
            Employees = employees;
        }

        public int TotalPassengerCapacity => Planes.Sum(p => p.PassengerCapacity);
        public double TotalCargoCapacity => Planes.Sum(p => p.CargoCapacity);
        private List<Plane> PlanesByMaxFlyDistance => Planes.OrderBy(p => p.MaxFlyDistance).ToList();
        public Plane GetPlaneByFuelConsumption(double fuelConsumptionMin, double fuelConsumptionMax)
        {
            return Planes
                .Where(p => p.FuelConsumption >= fuelConsumptionMin)
                .Where(p => p.FuelConsumption <= fuelConsumptionMax)
                .FirstOrDefault();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Name - {Name}\n");
            foreach (var p in Planes)
            {
                sb.Append(p);
                sb.Append("\n");
            }
            return sb.ToString();
        }

        public string GetSortedPlanes()
        {
            var sortedPlanes = PlanesByMaxFlyDistance;
            StringBuilder sb = new StringBuilder();
            sb.Append($"Name - {Name}\n");
            foreach (var p in sortedPlanes)
            {
                sb.Append(p);
                sb.Append("\n");
            }
            return sb.ToString();
        }
    }
}
