﻿namespace AirCompany.Employees
{
    public enum SkillLevel
    {
        NoSkill,
        Trainee,
        Beginner,
        Experienced,
        Advanced,
        Expert
    }
}
